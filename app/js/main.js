var wH = (window.innerHeight || document.documentElement.clientHeight);
var wW = (window.innerWidth || document.documentElement.clientWidth);

var body = document.body,
	html = document.documentElement;

var pageHeight = Math.max(body.scrollHeight, body.offsetHeight,
	html.clientHeight, html.scrollHeight, html.offsetHeight);

var userAgent = navigator.userAgent.toLowerCase(),

	isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
	isTouch = "ontouchstart" in window,

	mobile = false;

if (isMobile || isTouch) {
	mobile = true;
}

var anchorScroll;

$(function() {
	var fitImages = document.querySelectorAll('.js-fit');
	objectFitImages(fitImages, {watchMQ: true});

	var $document = $(document),
		$window = $(window),
		$html = $("html"),
		$body = $("body");

	//Prevent dragging img, links, buttons
	$("img, a, button").on("dragstart", function(event) {
		event.preventDefault();
	});

	//Sticky
	var $stickyEl = $('.js-sticky');
	Stickyfill.add($stickyEl);

	//Scroll
	$window.scroll(scrollThrottler);

	var scrollTimeout;

	function scrollThrottler() {
		if (!scrollTimeout) {
			scrollTimeout = setTimeout(function() {
				scrollTimeout = null;
				var scrollTop = $window.scrollTop();
				// pageScrollActionHandler();

			}, 100);
		}
	}

	function pageScrollActionHandler() {
		// var scrollTop = $window.scrollTop();
		//
		// if (scrollTop >= 100) {
		//    
		// } else {
		//    
		// }
	}

	//Card lazy load
	var lazyLoadImg = function($img) {
		$img.attr('src', $img.attr('data-src'));

		$img[0].onload = function() {
			$img.prop('data-src', false);
			$img.addClass('swiper-lazy-loaded').removeClass('swiper-lazy');
			$img.next('.loader').remove()
		};
	};

	var $imgNotSlider = $('.swiper-lazy').filter(function(index) {
		return !$(this).closest('.swiper-container').length || null;
	});

	$imgNotSlider.each(function() {
		lazyLoadImg($(this));
	});

	//Anchor link
	$('.js-anchor-link').on('click', function(e) {
		e.preventDefault();

		var offset = $('.js-single-masthead').length ? $('.js-single-masthead > div').height() : 0;

		anchorScroll($(this).attr('href'), 600, offset);
	});

	anchorScroll = function(elName, time, offset) {
		var targetOffset = elName ? +$(elName).offset().top : 0;

		if (!!offset) {
			targetOffset -= offset;
		}

		$('body, html').animate({scrollTop: targetOffset}, time || 600);
	};
	
	//Popup
	var openPopup = function(popupSrc) {
		$.fancybox.close();

		$.fancybox.open({
			src: popupSrc,
			type: 'inline',
			opts: {
				animationEffect: "fade",
				animationDuration: 400,
				toolbar: false,
				smallBtn: false,
				autoFocus: true,
				touch: false,
				baseTpl:
					'<div class="fancybox-container" role="dialog" tabindex="-1">' +
					'<div class="fancybox-bg"></div>' +
					'<div class="fancybox-inner">' +
					'<div class="fancybox-stage"></div>' +
					"</div>" +
					"</div>",
			}
		}, {
			baseClass: 'fancybox-popup',
			beforeShow: function(instance, slide) {

			},
			afterClose: function(instance, slide) {

			},
		});
	};

	window.openPopup = openPopup;

	$('.js-popup-link').on('click', function(e) {
		e.preventDefault();
		var popupSrc = $(this).attr('href');
		openPopup(popupSrc);
	});

	$('.js-popup-close').on('click', function(e) {
		e.preventDefault();
		$.fancybox.close();
	});

	//dropdown
	$('.js-dropdown-toggle').on('click', function() {
		$(this).closest('.js-dropdown').toggleClass('open');
	});

	$(document).on('keydown', function(e) {
		if (e.keyCode === 27 || e.which === 27) {
			$('.js-dropdown').removeClass('open');
		}
	});

	var prevDropdown = null;

	$(document).on('click', function(e) {
		var currentDropdown;

		if ($(e.target).hasClass('js-dropdown')) {
			currentDropdown = $(e.target);
		} else if ($(e.target).closest('.js-dropdown').length) {
			currentDropdown = $(e.target).closest('.js-dropdown');
		}
		if (!currentDropdown) {
			$('.js-dropdown').removeClass('open');
		} else {
			if (!currentDropdown.is(prevDropdown) && prevDropdown) {
				prevDropdown.removeClass('open');
			}
			prevDropdown = currentDropdown;
		}

	});

	$('.js-dropdown-link').on('click', function(e) {
		e.preventDefault();
		var dropdown = $(this).closest('.js-dropdown');

		if (!$(this).hasClass('active')) {
			dropdown.find('.js-dropdown-toggle').html($(this).html());
			dropdown.find('.js-dropdown-link.active').removeClass('active');
			$(this).addClass('active');
		}

		dropdown.removeClass('open');
	});

	//Slider
	var slidesNumber = $('.js-slider-reviews').find('.swiper-slide').length;

	var sliderReviews = new Swiper('.js-slider-reviews', {
		loop: true,
		autoHeight: true,
		slidesPerView: 1,
		spaceBetween: 10,
		loopedSlides: slidesNumber,
		centeredSlides: true,

		breakpoints: {
			768: {
				spaceBetween: 24,
				slidesPerView: 'auto',
			},
			// 1024: {
			//     slidesPerView: 5,
			//     spaceBetween: 50,
			// },
		},

		// Navigation arrows
		navigation: {
			nextEl: '.js-slider-reviews-next',
			prevEl: '.js-slider-reviews-prev',
		},
	});

	//Accordion
	var handleBenefits = function() {
		var windowWidth = $(window).width();
		if (windowWidth < 768) {
			$('.js-benefits-item-header').on('click', function() {
				$(this).closest('.js-benefits-item').toggleClass('open');
			});
		} else {
			$('.js-benefits-item-header').off('click');
		}
	};
	handleBenefits();

	$(window).on('resize', handleBenefits);
	
	//Tabs
	$('.js-tabs-nav').on('click', function(e) {
		e.preventDefault();
		var tabs = $(this).closest('.js-tabs');
		var currentId = $(this).attr('data-nav-id');
		var currentSrc = $(this).attr('data-content-src');

		//nav
		tabs.find('.js-tabs-nav').removeClass('active');
		$(this).addClass('active');
		
		//content
		var prevContent = tabs.find('.js-tabs-content.active');
		var currentContent = tabs.find('.js-tabs-content[data-content-id="' + currentId + '"]');
		
		tabs.find('.js-tabs-content').removeClass('active loaded');
		prevContent.find('iframe').remove();
		currentContent.addClass('active');
		
		//start loading iframe
		loadIframe(currentSrc, currentContent);
	});

	var loadIframe = function(src, container) {
		var iframe = document.createElement('iframe');
		iframe.onload = function() {
			setTimeout(function() {
				container.addClass('loaded').removeClass('loading');
			}, 300);
    };
		
		iframe.src = src;
		$(iframe).attr('scrolling', 'no');
		$(iframe).attr('marginwidth', '0');
		$(iframe).attr('marginheight', '0');
		$(iframe).attr('frameborder', '0');
		$(iframe).attr('border', '0');
		$(iframe).addClass('tabs__content-frame js-fit');
		container.append($(iframe));
		container.addClass('loading');
	};

	loadIframe($('.js-tabs-nav.active').attr('data-content-src'), $('.js-tabs-content.active'));
});
