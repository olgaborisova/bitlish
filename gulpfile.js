var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    terser = require('gulp-terser'),
    sass = require('gulp-sass'),
    cssmin = require('gulp-minify-css'),
    useref = require('gulp-useref'),
    gulpIf = require('gulp-if'),
    rimraf = require('rimraf'),
    fileinclude = require('gulp-file-include'),
    browserSync = require("browser-sync"),
    plumber = require('gulp-plumber'),
    reload = browserSync.reload,
    responsive = require('gulp-responsive');


// Config
var path = {
    build: {
        html: 'www/',
        js: 'www/js/',
        css: 'www/css/',
        img: 'www/images/',
        fonts: 'www/fonts/'
    },
    src: {
        main: './app',
        html: 'app/*.html',
        htmltemplate: 'app/template/*.html',
        htmltemplatelayout: 'app/template/layout/*.html',
        js: 'app/js/**/*.js',
        // scripts: 'app/js/*.js',
        css: 'app/css/',
        cssfile: 'app/css/**/*.css',
        style: 'app/scss/**/*.scss',
        img: 'app/images/**/*.*',
        imgToOptimize: 'app/images/**/*.{jpg,jpeg,png}',
        imgNoOptimize: 'app/images/**/*.svg',
        fonts: 'app/fonts/**/*.*'
    },
    watch: {
        html: 'app/**/*.html',
        js: 'app/js/**/*.js',
        scripts: 'app/js/*.js',
        style: 'app/scss/**/*.scss',
        htmltemplate: 'app/template/*.html',
        htmltemplatelayout: 'app/template/layout/*.html',
    },
    clean: './www'
};

var config = {
    server: {
        baseDir: "./app"
    },
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "bitlish"
};

// Development Tasks

// Запуск webserver (browserSync)
gulp.task('webserver', function (done) {
    browserSync(config);
    done();
});

// Очистка
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

// sass
function sassBuild() {
    return gulp.src(path.src.style)
        .pipe(plumber())
        .pipe(sass({
            // outputStyle: 'compressed',
            indentWidth: 4
        }))
        .pipe(plumber())
        .pipe(prefixer({
            browsers: ['last 10 versions'],
            cascade: false,
            remove: false
        }))
        //.on('error', console.log)
        .pipe(gulp.dest(path.src.css))
        .pipe(reload({stream: true}));
}

//fileinclude
gulp.task('fileinclude', function() {
    gulp.src([path.src.htmltemplate])
        .pipe(fileinclude())
        .pipe(gulp.dest('./app/'))
        .pipe(reload({stream: true}));
});

function fileincludeBuild() {
    return gulp.src([path.src.htmltemplate])
        .pipe(fileinclude())
        .pipe(gulp.dest('./app/'))
        .pipe(reload({stream: true}));
}

// html
gulp.task('html:build', gulp.series('fileinclude', function() {
    gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(gulp.dest(path.src.main))
        .pipe(reload({stream: true}));
}));

// js & style
gulp.task('useref', function() {
    return gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(useref())
        .pipe(gulpIf(path.src.cssfile, prefixer({
            browsers: ['last 10 versions'],
            cascade: false,
            remove: false
        })))
        .pipe(gulpIf('*.css', cssmin({processImport: false})))
        .pipe(gulpIf('*.js', terser()))
        .pipe(gulp.dest(path.build.html));      
});

// images
gulp.task('image:build', function(done) {
    gulp.src(path.src.imgToOptimize)
      .pipe(
        responsive({
            '**/*.{png,jpg}': [
                {
                    quality: 80,
                    progressive: true,
                    skipOnEnlargement: true,
                    errorOnUnusedConfig: false,
                    errorOnUnusedImage: false,
                    errorOnEnlargement: false,
                },
            ],
        })
      )
      .pipe(gulp.dest(path.build.img));

    gulp.src(path.src.imgNoOptimize)
      .pipe(gulp.dest(path.build.img));
    done();
});

// fonts
gulp.task('fonts:build', function(done) {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
    done();
});

// scripts
// gulp.task('scripts:build', function(done) {
//     gulp.src(path.src.scripts)
//         .pipe(gulp.dest(path.build.js));
//     done();
// });

// Watchers
gulp.task('watch', function() {
    gulp.watch(path.watch.style, sassBuild);
    gulp.watch(path.watch.htmltemplatelayout, fileincludeBuild);
    gulp.watch(path.watch.htmltemplate, fileincludeBuild);
    gulp.watch(path.watch.scripts, reload);
});


gulp.task('default', gulp.parallel('watch', 'webserver', function(done) {
    done();
}));

//Build
gulp.task('build', gulp.series('useref', 'image:build', 'fonts:build', function(done) {
    done();
}));
